/**
 * This is a description of ChildL31 class.
 *
 * @class
 * @augments ChildL21
 *
 * @param a "a"
 */
function ChildL31(a) {}

/**
 * Root
 * @class
 * @augments SuperRoot
 */
function Root() {}
/**
 * property
 *
 * @type {Number}
 */
Root.prototype.prop = 8;
/**
 * Method
 *
 * @param {String} str - String
 */
Root.prototype.method = function(){};
/**
 * Static property
 *
 * @type {Number}
 */
Root.staticV = 5;

/**
 * Some namespace.
 * @namespace
 */
const ns = {};
/**
 * Class A in the namespace.
 */
ns.A = class {};

/**
 * Class B in the namespace that extends the class A in the same namespace.
 *
 * @extends ns.A
 * @memberof ns
 *
 * @param {String} a "a".
 */
class B extends ns.A {
	constructor(a) {
		super();
	}
};

ns.B = B

/**
 * Class C.
 *
 * @class C
 * @extends ns.B
 * @memberof ns
 *
 * @param a "a"
 * @param b "b"
 */
class C extends B {
	constructor(a, b) {
		super(a);
	}
}

ns.C = C;
