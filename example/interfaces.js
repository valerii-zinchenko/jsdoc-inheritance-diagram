/**
 * Some interface.
 * @interface ISomething
 */

/**
 * Some method to implement.
 * @function
 * @name ISomething#doSomething
 */

/**
 * Child interface.
 * @interface IChildSomething
 * @augments ISomething
 */

/**
 * Some method to implement.
 * @function
 * @name IChildSomething#doSomethingElse
 */

/**
 * Some class, that implements some interface.
 *
 * @implements ISomething
 */
class SomeClassImplementsISomething {}

/**
 * Some class, that implements some interface with parent.
 *
 * @augments SomeClassImplementsISomething
 * @implements IChildSomething
 */
class AnotherClassImplementsIChildSomething extends SomeClass {}
