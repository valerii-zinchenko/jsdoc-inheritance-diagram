/*
 * Copyright (c) 2017-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/jsdoc-inheritance-diagram/blob/master/LICENSE.txt)
 * All source files are available at: https://gitlab.com/valerii-zinchenko/jsdoc-inheritance-diagram
 */


const env = require('jsdoc/env');
let diagramConf = env.conf['inheritance-diagram'];

if (!diagramConf) {
	diagramConf = {};
	env.conf['inheritance-diagram'] = diagramConf;
}
env.opts['inheritance-diagram'] = diagramConf;

// Cache a defined template to be run after diagrams are generated. Fallback template name is taken from jsdoc/cli.js::generateDocs()
diagramConf.__template = env.opts.template;
env.opts.template = 'jsdoc-inheritance-diagram/template';


let isDocForExternalLinksGenerated = !diagramConf.externalLinks;
exports.handlers = {
	beforeParse: function(e) {
		// this event is fired for each file but additional documentation must be added only once to an event
		if (isDocForExternalLinksGenerated) {
			return;
		}

		let extraDoc = '';
		Object.keys(diagramConf.externalLinks).forEach(key => {
			extraDoc += `/**
 * This is an automatically generated documentation page by <a href="https://www.npmjs.com/package/jsdoc-inheritance-diagram" target="_blank"><code>jsdoc-inheritance-diagram</code></a> plugin. Please follow the "see" link to see more details about this.
 * @class ${key}
 * @see ${diagramConf.externalLinks[key]}
 */`;
		});

		e.source += extraDoc;

		isDocForExternalLinksGenerated = true;
	}
};
