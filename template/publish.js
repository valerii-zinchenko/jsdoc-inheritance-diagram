/*
 * Copyright (c) 2017-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/jsdoc-inheritance-diagram/blob/master/LICENSE.txt)
 * All source files are available at: https://gitlab.com/valerii-zinchenko/jsdoc-inheritance-diagram
 */


const logger = require('jsdoc/util/logger');
const path = require('jsdoc/path');
const env = require('jsdoc/env');
const helper = require('jsdoc/util/templateHelper');

const diagramBuilder = require('inheritance-diagram');
let map = {};

function addClass(doclet) {
	const name = doclet.longname;
	const node = {
		children: [],
		link: `${name}.html`,
		// Reference to a doclet is needed here to modify its description with a diagram
		doclet: doclet
	};

	const {augments, mixes} = doclet;
	const impls = doclet.implements;

	// If class does not extends any other class, then 'augments' property does not exist
	if (augments && augments.length > 0) {
		// inheritance-diagram lib does not supports multiple parents
		node.parent = augments[0];
	}

	// If class does not implements any interface, then 'implements' property does not exist
	if (impls && impls.length > 0) {
		node.implements = [...impls];
	}

	// If class does not mixes anything, then 'mixes' property does not exist
	if (mixes && mixes.length > 0) {
		node.mixes = [...mixes];
	}

	map[name] = node;

	return node;
}


exports.publish = async (taffyData, opts, tutorials) => {
	logger.info('Generating inheritance diagrams...');

	const diagramConf = opts['inheritance-diagram'];

	const data = helper.prune(taffyData);
	const members = helper.getMembers(data);

	members.classes.forEach(addClass);
	members.interfaces.forEach(addClass);

	// Populate children
	for (const name in map) {
		const node = map[name];
		const parentNode = map[node.parent];

		if (!parentNode) {
			continue;
		}

		if (parentNode.children.indexOf(name) === -1) {
			parentNode.children.push(name);
		}
	}

	for (const name in map) {
		// Generate inheritance diagrams
		const diagram = await diagramBuilder(name, map, diagramConf);

		const doclet = map[name].doclet;
		doclet.classdesc = `<div class="class-diagram">${diagram}</div>${doclet.classdesc || ''}`;
	}

	const templatePath = (() => {
		const publish = diagramConf.__template || 'templates/default';
		const templatePath = path.getResourcePath(publish);

		// if we didn't find the template, keep the user-specified value so the error message is
		// useful
		return templatePath || env.opts.template;
	})();
	env.opts.template = templatePath;

	let template;
	try {
		template = require(`${templatePath}/publish`);
	} catch (e) {
		logger.fatal(`Unable to load template: ${e.message}` || e);
	}

	if (template.publish && typeof template.publish === 'function') {
		logger.info('Continue generating output files...');
		return await template.publish(taffyData, opts, tutorials);
	} else {
		logger.fatal(`${env.opts.template} does not export a "publish" function. Global "publish" functions are no longer supported.`);
	}

	return;
};
